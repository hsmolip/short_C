Proyecto educacional de aprendizaje C y C++

Obra : short** (*c)++;
Autor : Humberto Molinares
Fecha : Jun 2018
Version : 0.0.0-1

Descripcion : Contamos con el gusto de invitarlos a seguir un curso de 
programacion en C y C++ de una forma detallada y vanguardista
de la cual estaremos viendo todos los temas que no son explicados en 
los estudios de carreras profesionales ni tecnicas.

Alcance : Dominar la meta-programacion en lenguaje C y C++ y desarrollar
un sistema operativo con el transcurrir del tiempo y del conocimiento

Agradecimiento : Para no ser desagradecidos hasta por la rasquinia en el
jopo Agradezco a DIOS-Todo Poderoso Jehova por la oportunidad de continuar
viendo cada caracter y poder decodificarlo en mi mente y entender a 
consciencia todo el contenido que estoy leyendo y el sentido que le doy
a la vida.